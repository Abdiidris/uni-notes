**Key**
|  Item | meaning  |
|---|---|
|  &#x1F4D7; | Confident
| &#x1F4D8; | moderate
| &#x1F4D9; | weak
---
# Table of content
- [Table of content](#table-of-content)
  - [PLC](#plc)
  - [Professional and Social Aspects](#professional-and-social-aspects)
  - [Operating Systems](#operating-systems)

   
---

## PLC
 
1. [**Introduction**](./plc/introduction.md) 
2. **Numbers and Numeric Types**
3. **Primitive Values**  
4. **Primitive Values**  
5. **Collections**  
6. **Recursive types**
7. **Variables**
8. **Subprograms**
9.  **Threads and Synchronisation**
10. **Modules**
11. **Support for Reliable Programming**

## Professional and Social Aspects
1. **Introduction**
2. **Moral, legal, and ethical framework**
3. **Ethics**
4. **Data protection**
5. **Intellectual property**
6. **Professionalism** 
7. **Research ethics and final-year projects**
8. **Association for Computing Machinery recommended syllabus**
   
## Operating Systems
1. [**Introduction to Operating Systems**](./os/introduction.md) &#x1F4D8;
2. [**OS Function and Structure**](./os/functions%20and%20structure.md) 
3. **Processes**
4. **Threads**
5. **CPU Scheduling**
6. **TBC**
7. **TBC**
8. **TBC**
9. **TBC**
10. **Main Memory**
11. **Virtual Memory**
12. **Storage Management File Systems**
13. **Storage Management Mass Storage Structure**
14. **History of Operating Systems**