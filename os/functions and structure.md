 [`` <-- ``](../overview.md) 

 [**Slides**](slides/untitled.pdf)

<hr/>

**Operating system Services**

<u>For the user</u>

   * UI: cli
   * File-system manipulation
   * IO services
   * Communication (networking)
   * Error detection
     * in user programs, hardware...
     * Appropriate action incase of error
     * Debugging to find errors
  
<u>For the efficiency</u>

*  Resource allocation
*  Resource logging (accounting)
*  Protection and security
   *   Protection: Of resources
   *   Security: From hackers through authentication
---
**Computer system operations**
![architecture](./images/Computer_system_architecture.png)
<u>Interrupt driven system</u>
 A signal sent to stop current execution and grab attention. The CPU saves it's state and starts executing the interrupt handler.

  *Hardware* trigger interrupts using a signal to the CPU
  *Software* uses a *System call* to trigger interrupts

---

**Dual Mode**
<br/>

  ![modes](./images/1_10_UserToKernelMode.jpg)

This adds security for the OS from other components as it ensures only it can execute 
calls.

---

**System calls**

I 