 [`` <-- ``](../overview.md) 

### Slides
* [plain](slides/CS2230-lecture1-answers.pdf)
* [with solutions](slides/CS2230-lecture1.pdf)

<hr>

**What is an operating system?**

1. A system that *virtualizes* the hardware and makes running programs possible.
2. A prgram that runs other programs
3. A *resource allocator* and *control program*
<br/>

**4 components of an computer system**

                       Hardware -----> OS -----> Applications -----> Users
   1. Hardware: CPU, memory..
   2. OS: controls the access of hardware resources to Applications and Users
   3. Applications: Programs that will be run on the os e.g. word
      1. Draw on the screen
      2. Interact via a keyboard
      3. mouse etc –Access the hard disk (files)
      4. Communicate with other application programs
   4. Users: can be machines and use the OS and Applications
   
The *kernal* is an OS program and is always running

*Objectives of an OS*
   1. Abstraction: Hide hardware details
   2. Managment: Manage resource allocation for processes
   3. UI: To make using the system easier

<hr>

**Organisation of a system**
    
*Bootstrap progam*

```A program that is stored in the ROM and loads the OS when the computer is turned on. It does this by starting the *kernal* which executes the first program e.g. init```

*interrupts*

```A signal sent by an I/O device that stops the CPUs current execution and execute another part of the OS```
<hr>

